package fr.alteca.cicd.service;

import fr.alteca.cicd.model.Employee;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

@SpringBootTest
class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void manageEmployees(){

        Employee employee = new Employee();
        employee.setId("1");
        employee.setExperience("1");
        employee.setFirstName("Ludovic");
        employee.setLastName("Franceschini");
        employee.setSkills(Arrays.asList("Java"));

        employeeService.createEmployee(employee);

        assertThat(employeeService.getEmployees(), is(not(nullValue())));
        assertThat(employeeService.getEmployees(), is(not(Matchers.empty())));

        Employee employee2 = new Employee();
        employee2.setId("2");
        employee2.setExperience("11");
        employee2.setFirstName("David");
        employee2.setLastName("Teite");
        employee2.setSkills(Arrays.asList("Data"));

        employeeService.createEmployee(employee2);

        assertThat(employeeService.getEmployees().size(), equalTo(2));

        employeeService.removeEmployee("1");

        assertThat(employeeService.getEmployees().size(), equalTo(1));

        Employee employeeFound = employeeService.findEmployeeById("2");

        assertThat(employeeFound.getId(), equalTo("2"));
        assertThat(employeeFound.getExperience(), equalTo("11"));
        assertThat(employeeFound.getFirstName(), equalTo("David"));
        assertThat(employeeFound.getLastName(), equalTo("Teite"));
        assertThat(employeeFound.getSkills().size(), equalTo(1));

    }

}