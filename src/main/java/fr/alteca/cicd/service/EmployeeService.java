package fr.alteca.cicd.service;

import fr.alteca.cicd.model.Employee;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

@Service
public class EmployeeService {

    private List<Employee> employees = new ArrayList<>();

    public void createEmployee(Employee employee) {
        if (employee != null) {
            employees.add(employee);
        }
    }

    public void removeEmployee(String id) {
        if (Strings.isNotBlank(id)) {
            OptionalInt index = IntStream
                    .range(0, employees.size())
                    .filter(i -> id.equals(employees.get(i).getId()))
                    .findFirst();
            index.ifPresent(i -> employees.remove(i));
        }
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee findEmployeeById(String id) {
        if (Strings.isNotBlank(id)) {
            return employees.stream().filter(employee -> id.equals(employee.getId())).findFirst().orElse(null);
        }
        return null;
    }

    public String test(){
        return "Pas coucou";
    }
}
