package fr.alteca.cicd.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Employee {

    private String id;
    private String firstName;
    private String lastName;
    private String experience;
    private List<String> skills;

}
