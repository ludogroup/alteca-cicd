package fr.alteca.cicd.controller;

import fr.alteca.cicd.model.Employee;
import fr.alteca.cicd.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/employees")
public class EmployeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    public void createEmployee(Employee employee) {
        employeeService.createEmployee(employee);
    }

    @DeleteMapping
    public void removeEmploye(String id) {
        employeeService.removeEmployee(id);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping(value = "/{id}")
    public Employee findEmployeeById(String id) {
        return employeeService.findEmployeeById(id);
    }


}
